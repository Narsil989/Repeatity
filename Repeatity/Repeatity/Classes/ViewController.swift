//
//  ViewController.swift
//  ListenOnRepeat
//
//  Created by Dejan Kraguljac on 18/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import UIKit
import Alamofire
import GoogleAPIClientForREST

class ViewController: UIViewController {
    
    var itemsArray: [GTLRYouTube_SearchResult] = []
    
    let tableView: UITableView = {
       
        let tableView = UITableView(frame: .zero)
        tableView.allowsMultipleSelection = false
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        let request = try URLRequest(url: URL(string: "https://www.googleapis.com/youtube/v3/search?part=snippet&q=xkito&key=AIzaSyAQ7xQOPuK-F7UMLPD5V7Dk4LdLGOQEsmE")!)
        Alamofire.request(request).responseJSON {[weak self] (response) in
            
            print(response)
            switch response.result {
            case .success(let value):
                let result = GTLRYouTube_SearchListResponse.init(json: value as! [AnyHashable : Any])
                if let itms = result.items {
                    self?.itemsArray = itms
                    self?.tableView.reloadData()
                }
                
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupTableView() {
        
        self.view.addSubview(self.tableView)
        self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(CustomCell.self, forCellReuseIdentifier: "CustomCell")
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomCell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomCell
        
        cell.configureUI(searchResult: self.itemsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(YTVideoSingleViewController(item: self.itemsArray[indexPath.row]), animated: true)
    }
    
    
}

