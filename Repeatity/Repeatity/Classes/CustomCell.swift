//
//  CustomCell.swift
//  ListenOnRepeat
//
//  Created by Dejan Kraguljac on 18/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import Foundation
import UIKit
import GoogleAPIClientForREST
import Kingfisher

final class CustomCell: UITableViewCell {
    
    let label: UILabel = {
       
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    let thumbnailImageView: UIImageView = {
       
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
        
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        
        self.contentView.addSubview(self.thumbnailImageView)
        self.thumbnailImageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        self.thumbnailImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        self.thumbnailImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.thumbnailImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.contentView.addSubview(self.label)
        self.label.leadingAnchor.constraint(equalTo: self.thumbnailImageView.trailingAnchor, constant: 15).isActive = true
        self.label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        self.label.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
    }
    
    func configureUI(searchResult: GTLRYouTube_SearchResult) {
        if let imageUrl = searchResult.snippet?.thumbnails?.medium?.url, let url = URL(string: imageUrl) {
            self.thumbnailImageView.kf.setImage(with: ImageResource(downloadURL: url))
        }
        if let text = searchResult.snippet?.title {
            self.label.text = text
        }
    }
    
}
