//
//  YTVideoSingleViewController.swift
//  ListenOnRepeat
//
//  Created by Dejan Kraguljac on 18/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import MediaPlayer
import GoogleAPIClientForREST
import youtube_ios_player_helper
import MediaPlayer

final class YTVideoSingleViewController: UIViewController, YTPlayerViewDelegate, Remotable {
    
    func handlePlayVideo() {
        
    }
    
    func handlePauseVideo() {
        
    }
    
    func handleNextVideo() {
        
    }
    
    func handlePreviousVideo() {
        
    }
    
    let ytPlayer: YTPlayerView = {
       
        let playerView = YTPlayerView(frame: .zero)
        playerView.translatesAutoresizingMaskIntoConstraints = false
        return playerView
        
    }()
    
    var appDidEnteredBackground: Bool = false
    
    var item: GTLRYouTube_ResourceId? {
        didSet{
            guard let tim = self.item, let id = tim.videoId else { return }
            
//            self.ytPlayer.load(withVideoId: id)
//            self.ytPlayer.playVideo()
        }
    }
    
    init(item: GTLRYouTube_SearchResult) {
        self.item = item.identifier
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        guard let tim = self.item, let id = tim.videoId else { return }
        
        let dict: Dictionary =  ["autoplay": 1, "fs": 0, "playsinline": 1]
        self.ytPlayer.load(withVideoId: id, playerVars: dict)
        self.ytPlayer.delegate = self
        self.ytPlayer.setLoop(true)//maybe if I make the playlist first xD
        self.view.addSubview(self.ytPlayer)
        
        
        self.ytPlayer.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.ytPlayer.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.ytPlayer.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.ytPlayer.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.height/3).isActive = true
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: "this is title", MPMediaItemPropertyArtist: "Artist", MPNowPlayingInfoPropertyElapsedPlaybackTime: 0, MPMediaItemPropertyPlaybackDuration: 60, MPNowPlayingInfoPropertyPlaybackRate: 1.0, MPNowPlayingInfoPropertyIsLiveStream: true]
        MPRemoteCommandCenter.shared().playCommand.addTarget(handler: {[weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.ytPlayer.playVideo()
            return .success
        })
        MPRemoteCommandCenter.shared().playCommand.isEnabled = true
        
        MPRemoteCommandCenter.shared().pauseCommand.addTarget(handler: {[weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.ytPlayer.pauseVideo()
            return .success
        })
        MPRemoteCommandCenter.shared().pauseCommand.isEnabled = true

        NotificationCenter.default.addObserver(self, selector: #selector(appEnteredBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appEnteredBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appBecameActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    
    
    override func remoteControlReceived(with event: UIEvent?) {
        
        guard let ev = event else { return }
        switch ev.type {
        case .remoteControl:
            print("remote control")
        default:
            break
        }
        
    }
    
    func appEnteredBackground() {
        self.appDidEnteredBackground = true
    }
    
    func appBecameActive() {
        self.appDidEnteredBackground = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.endReceivingRemoteControlEvents()
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        playerView.playVideo()

    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        case .playing:
            print("playing")
        case .ended:
            print("ended")
            playerView.playVideo()
        case .buffering:
            print("buffering")
        case .paused:
            print("buffering")
            // if self.appDidEnteredBackground == true {
              //  self.ytPlayer.playVideo()
           // }
        case .queued:
            print("queued")
        case .unstarted:
            print("unstarted")
        default:
            print("none")
        }
    }
}
