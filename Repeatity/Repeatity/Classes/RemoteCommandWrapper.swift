//
//  RemoteCommandWrapper.swift
//  ListenOnRepeat
//
//  Created by Dejan Kraguljac on 01/02/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import Foundation
import MediaPlayer
import youtube_ios_player_helper

public protocol Remotable {
    
    func handlePlayVideo()
    func handlePauseVideo()
    func handleNextVideo()
    func handlePreviousVideo()
    
}

final class RemoteCommandWrapper: NSObject {
    
    
    fileprivate let commandCenter = MPNowPlayingInfoCenter.default()
    
    var ytPlayer = YTPlayerView()
    
    init(videoId: String) {
        self.ytPlayer.load(withVideoId: videoId, playerVars: nil)
    }
    
}
